# Repository

This repository contains LittlevGL fonts and images converters.
The image converter was modified and supports Floyd-Steinberg dithering algorithm,
also small improvements in code were added.

- The original source code of utilities can be found here:
<https://github.com/littlevgl/lv_utils>

- Description of Floyd-Stainberg algorithm and several other olgorithms, can be found here:
<http://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/>

# Floyd-Steinberg dithering algorithm

At the examples below you can see difference between original (left) and modified converter with dithering (right).

  - LV_COLOR_DEPTH 8
 
 ![](./example_images/img1_8.jpg) ![](./example_images/img1-dith_8.jpg)
  - LV_COLOR_DEPTH 16
 
 ![](./example_images/img1_16.jpg) ![](./example_images/img1-dith_16.jpg)
  - LV_COLOR_DEPTH 24
 
 ![](./example_images/img1_24.jpg) ![](./example_images/img1-dith_24.jpg)

# Other changes

- Improved pixels classification

In original converting script bit mask was used to classify pixels. 
Due to that they always be rounded down. More precise way of classification is to put the pixel to the closest possible value.

- Improved strings comparsion

Better way of comparing strings in PHP is to use strcmp() function then == operator.

# Usage

Dithering is enabled by default. If you want to disable it just add apropriate argument `dith=disable`: <br />
```
php img_conv_core.php "name=icon&img=bunny.png&format=c_array&dith=disable"
```

# Info

Code appended to original LittlevGL library in:  
[https://github.com/littlevgl/lv_utils/pull/8](https://github.com/littlevgl/lv_utils/pull/8)